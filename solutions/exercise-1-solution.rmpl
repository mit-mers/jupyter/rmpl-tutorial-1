;;;; -*- Mode: common-lisp; -*-

(defpackage #:morning-rush)

(in-package #:morning-rush)

(define-control-program shower ()
  (declare (primitive)
           (duration (simple :lower-bound 5 :upper-bound 10))))

(define-control-program eat-breakfast ()
  (declare (primitive)
           (duration (simple :lower-bound 15 :upper-bound 20))))

(define-control-program review-notes ()
  (declare (primitive)
           (duration (simple :lower-bound 10 :upper-bound 15))))

(define-control-program finish-homework ()
  (declare (primitive)
           (duration (simple :lower-bound 10 :upper-bound 15))))

(define-control-program pack-bag ()
  (declare (primitive)
           (duration (simple :lower-bound 5 :upper-bound 5))))

(define-control-program bike-to-school ()
  (declare (primitive)
           (duration (simple :lower-bound 15 :upper-bound 20))))

(define-control-program morning-work ()
  (sequence ()
    (review-notes)
    (finish-homework)))

(define-control-program main ()
  (with-temporal-constraint (simple-temporal :upper-bound 60)
    (sequence ()
      (shower)
      (parallel (:slack t)
        (eat-breakfast)
        (morning-work))
      (pack-bag)
      (bike-to-school))))
