import json, subprocess
from src.scheduler import *
from IPython.display import display, display_html, Javascript, YouTubeVideo
from IPython.core.display import HTML

def display_video(url, **kwargs):
    """
    Displays a Youtube video in a Jupyter notebook.
    
    Args:
        url (string): a link to a Youtube video.
        **kwargs: further arguments for IPython.display.YouTubeVideo
    
    Returns:
        YouTubeVideo: a video that is displayed in your notebook.
    """
    id_ = url.split("=")[-1]
    return YouTubeVideo(id_, **kwargs)

# TPN visualization Javascript
TPN_JS = """
require("notebook/js/outputarea").OutputArea.prototype._should_scroll = function(lines) {
    return false;
}

var tinycolor;

require.config({
  paths: {
      d3: 'https://d3js.org/d3.v5.min',
      dagreD3: 'https://dagrejs.github.io/project/dagre-d3/latest/dagre-d3'
  }
});

requirejs(['d3', 'dagreD3'], function(d3, dagreD3){

var __PS_MV_REG;

//////////

function maybeInf(num) {
    if (num == 'Infinity') {
        return '∞';
    } else if (num == '-Infinity') {
        return '-∞';
    } else if (num === 0.001) {
        return 'ε';
    } else {
        return num;
    };
};
function constructStatePlanGraph(sp) {
    var g = new dagreD3.graphlib.Graph().setGraph({  });
    var events1 = sp.stateSpace.events;
    var constraints2 = sp.constraints;
    var episodes3 = sp.episodes;
    g.graph().rankDir = 'LR';
    events1.forEach(function (event) {
        return g.setNode(event.$id, { label : '', shape : 'circle' });
    });
    constraints2.forEach(function (c) {
        /*
        if (c.$annotations['causal-link']) {
            console.log('CAUSAL LINK');
        };
        */
        var expression4 = c.expression;
        if (expression4.$type == 'simpleTemporalApplication') {
            __PS_MV_REG = [];
            return g.setEdge(expression4.from.ref, expression4.to.ref, { label : '[' + maybeInf(expression4.lowerBound) + ',' + maybeInf(expression4.upperBound) + ']', 'class' : 'constraint' });
        };
    });
    episodes3.forEach(function (ep) {
        var label = '(' + ep.activityName + ')';
        /*
        ep.activityArgs.forEach(function (arg) {
            return label = label + ' ' + arg;
        });
        */

        label += '[' + maybeInf(ep.duration.lowerBound) + ',' + maybeInf(ep.duration.upperBound) + ']'

        return g.setEdge(ep.startEvent, ep.endEvent, { label : label, 'class' : 'episode' });
    });
    return g;
};
function displayStatePlan(sp, svg) {
    var svgInner = svg.select('g');
    var zoom5 = d3.zoom().on('zoom', function () {
        return svgInner.attr('transform', d3.event.transform);
    });
    var render6 = new dagreD3.render();
    var g = constructStatePlanGraph(sp);
    var initialScale = 0.75;
    svg.call(zoom5);
    __PS_MV_REG = [];
    return render6(svgInner, g);
};

///////////

window.displayStatePlan = function(sp, svgId) {
    displayStatePlan(sp, d3.select(svgId));
}
});

"""

# TPN visulization style
TPN_CSS = """
<style>
svg {
    font-family: "Lucida Console", Monaco, monospace;
}
.node circle {
  stroke: #333;
  fill: #fff;
}
.constraint path {
  stroke: #333;
  fill: #333;
  stroke-width: 1.5px;
}
.episode path {
  stroke: #0f0;
  fill: #0f0;
  stroke-width: 3px;
}
/*.causal-link path {
  stroke: #00f;
  fill: #00f;
  stroke-width: 3px;
}
.threat path {
  stroke: #f00;
  fill: #f00;
  stroke-width: 3px;
}*/
</style>
"""

SCH_JS="""
require("notebook/js/outputarea").OutputArea.prototype._should_scroll = function(lines) {
    return false;
}

var tinycolor;

require.config({
  paths: {
      sha1: 'http://editor.planning.domains/plugins/featured/timeline-viewer/sha1.js',
      charts: 'https://www.gstatic.com/charts/loader.js'
  }
});

requirejs(['https://www.gstatic.com/charts/loader.js'],
          function() {
              google.charts.load('current', {packages: ['timeline']});
         });

requirejs(['https://courses.csail.mit.edu/16.410/js/sha1.js'],
          function() {
              console.log('Loaded SHA1');
         });

requirejs(['https://courses.csail.mit.edu/16.410/js/tinycolor.js'],
          function(m) {
              console.log('Loaded tinycolor');
              tinycolor = m;
         });

function get_terms(pred) {
  // Cut off the first and last characters - the parenthesis
  pred = pred.trim().slice(1, pred.length - 1);
  var words = pred.split(" ");
  return words;
}

function wordToColor(word, sat, lightness) {
  // Helper function that converts a word to a color based on the SHA1 sum
  var sha1_word = CryptoJS.SHA1(word).words[0];
  sha1_word = sha1_word % 360;
  if (sha1_word < 0) {
    sha1_word += 360;
  }
  var hsl_string = 'hsl(' + sha1_word + ", " + (100 * sat) + "%, " + (100 * lightness) + "%)";
  return '#' + tinycolor(hsl_string).toHex();
}

function activity_to_table_row(match, id) {
  var start_time = 1000*parseFloat(match[1]);
  var end_time = 1000*parseFloat(match[3]) + start_time;
  return ["" + id, match[2],  start_time, end_time];
}

function activity_to_color(match) {
  var words = get_terms(match[2]);
  var action_name = words[0].trim();
  var color = wordToColor(action_name, 0.7, 0.75);
  return color;
}

function display_timeline(chart_div, plan_string) {
    requirejs(['https://www.gstatic.com/charts/loader.js'],
        function() {
            google.charts.load('current', {packages: ['timeline']});
            google.charts.setOnLoadCallback(function () {display_timeline_cb(chart_div, plan_string)});
        });
}

window.display_timeline = display_timeline;

function display_timeline_cb(div_name, plan_string){
    // Add the google chart
    var options = {
      height: 100,
      animation: {
        duration: 1000,
        easing: 'out',
      },
      hAxis: {
        gridlines: {count: 15}
      },
      timeline: {
        showRowLabels: true,
        groupByRowLabel: false,
        colorByRowLabel: false,
      }
    };
    var chart = new google.visualization.Timeline(document.getElementById(div_name));
    
    // Get planner texxt
    var planner_output = plan_string;
    // Find all matches for lines that appear to be temporal actions
    var regexp = /^([\d.]+)\s*:\s*(\(.*\))\s*\[([\d.]+)\]$/gm;
    var matches = [];
    var match;
    while ((match = regexp.exec(planner_output)) !== null) {
      matches.push(match);
    }

    // Load the data into a table
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Task ID');
    data.addColumn('string', 'Task Name');
    data.addColumn('number', 'Start');
    data.addColumn('number', 'End');
    data.addRows([]);
    var colors_for_activities = [];
    for(var i = 0; i < matches.length; i++) {
        var match = matches[i];
        data.addRows([activity_to_table_row(match, i)])
        var color = activity_to_color(match)
        colors_for_activities.push(color);
    }

    // Draw the chart!
    options.height = data.getNumberOfRows() * 43 + 100;
    options.colors = colors_for_activities;
    chart.draw(data, options);
}
"""

def prep_browser():
    display(Javascript(TPN_JS))
    display(Javascript(SCH_JS))
    display(HTML(TPN_CSS))

num_sps = 0
num_divs = 0

def test_viz_stateplan(sp_json):
    global num_sps
    with open(sp_json) as f:
        plan = json.load(f)
    num_sps += 1
    display_html("""<div class="alert alert-success">
                 <strong>Plan found!</strong>
                 </div>""", raw=True)
    display_html('<div><svg width=100% height=300 id="sp-viz-' + str(num_sps) + '"><g/></svg></div>',
                 raw=True)
    blob = json.dumps(plan)
    display_html('<script> window.displayStatePlan(' + blob +
                 ', "#sp-viz-' + str(num_sps) + '") </script>',
                 raw=True)

def compile_rmpl(rmpl_file, output_plan="tpn.json"):
    global num_sps
    p = subprocess.run(['rmplc', rmpl_file], stdout=subprocess.PIPE)
    try:
        plan = json.loads(p.stdout)
    except:
        print("Invalid or empty RMPL model. Check your RMPL script and try again. If you get stuck, take a look at the solutions!")
        return
    
    with open(output_plan, 'w') as f:
        json.dump(plan, f)
    num_sps += 1
    display_html("""<div class="alert alert-success">
                 <strong>Plan found!</strong>
                 </div>""", raw=True)
    display_html('<div><svg width=100% height=300 id="sp-viz-' + str(num_sps) + '"><g/></svg></div>',
                 raw=True)
    blob = json.dumps(plan)
    display_html('<script> window.displayStatePlan(' + blob +
                 ', "#sp-viz-' + str(num_sps) + '") </script>',
                 raw=True)


def schedule_and_viz_plan(sp_json):
    global num_divs
    start_event, stn = create_stn(sp_json)

    schedule = schedule_plan(start_event, stn)

    # Create plan string
    plan_string = ''
    for (s, e, a) in stn.edges.data('activity', default=None):
        if a is not None:
            activity_str = ''
            activity_str += str(schedule[s]) + ': '
            activity_str += '(' + a + ') '
            activity_str += '[' + str(schedule[e] - schedule[s]) + ']\\n'
            plan_string += activity_str

    print("Schedule: \n")
    print(json.dumps(prettify_schedule(start_event, stn, schedule), indent=1))

    num_divs += 1
    display_html("""<div class="alert alert-success">
                 <strong>Schedule found!</strong>
                 </div>""", raw=True)
    display_html('<div id="timeline-graph-' + str(num_divs) + '"></div>',
                 raw=True)
    display_html('<script> window.display_timeline("timeline-graph-' + str(num_divs) + '", "' + plan_string + '") </script>',
                 raw=True)

    