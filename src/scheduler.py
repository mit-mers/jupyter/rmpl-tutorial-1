import json
import networkx as nx
import numpy as np

def maybe_inf(bound):
    return np.inf if bound == "Infinity" else bound 

# Create network from state plan
def create_stn(sp_json):
    """Converts a JSON state plan to a DiGraph for scheduling.

    Input: JSON state plan file
    
    Output: stn - a networkx DiGraph with simple temporal constraints
           start_event - a string, the event name in stn of the start event.
                         This start event should always be scheduled at t=0.
                         
    """
    # Load state plan
    with open(sp_json) as f:
        plan = json.load(f)

    stn = nx.DiGraph()

    # add edges from constraints
    constraints = plan['constraints']
    for c in constraints:
        expr = c['expression']
        stn.add_edge(expr['from']['ref'], expr['to']['ref'], 
            stc=[expr['lowerBound'], maybe_inf(expr['upperBound'])], activity=None)

    # add edges from episodes
    episodes = plan['episodes']
    for ep in episodes:
        stn.add_edge(ep['startEvent'], ep['endEvent'], 
            stc=[ep['duration']['lowerBound'], maybe_inf(ep['duration']['upperBound'])], 
            activity=ep['activityName'])

    # get start event
    start_event = plan['startEvent']

    return start_event, stn

def convert_stn_to_distance_graph(stn):
    """Converts an STN to a distance graph.
    
    Input: stn - an STN (namely, an nx.DiGraph, with stc=... set for every edge)
    Output: dg - a distance graph representation of stn
    """
    dg = nx.DiGraph()
    dg.add_nodes_from(stn.nodes()) # optional with nx
    # Now, add edges
    for (u, v) in stn.edges():
        lb, ub = stn[u][v]['stc']
        dg.add_edge(u, v, weight=ub)
        dg.add_edge(v, u, weight=-lb)
    return dg

def compute_apsp_dispatchable_form(dg):
    """Converts a distance graph, g, to a dispatchable form.
    
    This method should compute the APSP of g, and then create
    and return a new graph over the same nodes in g but with the
    edges from the APSP in the graph. It should include every edge EXCEPT for
    self-loop edges from a node to itself.
    
    Input: dg - a distance graph represented as a `networkx` graph.
    Output: g_apsp - a distance graph representing the APSP dispatchable form of dg
    """
    # Compute an APSP
    d = nx.floyd_warshall(dg)
    # Set up and return an APSP distance graph
    g_apsp = nx.DiGraph()
    g_apsp.add_nodes_from(dg.nodes()) # optional with nx
    for u in dg.nodes():
        for v in dg.nodes():
            if u != v:
                g_apsp.add_edge(u, v, weight=d[u][v])
    return g_apsp
    

class ExecutionWindow():
    def __init__(self, lb=-np.inf, ub=np.inf):
        self.lb = lb
        self.ub = ub

    def __str__(self):
        return "<Execution window [{}, {}]>".format(self.format_num(self.lb), self.format_num(self.ub))

    def format_num(self, v):
        if v == np.inf:
            return "∞"
        elif v == -np.inf:
            return "-∞"
        else:
            return str(v)

    __repr__ = __str__

def propagate_to_neighbors(e, t, g_disp, execution_windows):
    """Propagates event e's execution time t to its neighbors' execution windows."""
    # Set self
    win = execution_windows[e]
    win.lb = t
    win.ub = t
    # Propagate over outgoing edges using upper-bound formula
    for (u, v) in g_disp.out_edges(e):
        win = execution_windows[v]
        win.ub = min(win.ub, t + g_disp[u][v]['weight'])
    # Propagate over incoming edges using the lower-bound formula
    for (u, v) in g_disp.in_edges(e):
        win = execution_windows[u]
        win.lb = max(win.lb, t - g_disp[u][v]['weight'])

def prettify_schedule(start_event, stn, schedule, start_event_name=None, end_event_name=None):
    # get end event
    end_event = max(schedule, key=schedule.get)

    pretty_schedule = {'start-plan': schedule[start_event], 'end-plan': schedule[end_event]}
    for (s, e, a) in stn.edges.data('activity', default=None):
        if a is not None:
            pretty_schedule['start-'+ a] = schedule[s]
            pretty_schedule['end-'+ a] = schedule[e]

    if start_event_name: pretty_schedule[start_event_name] = pretty_schedule.pop('start-plan')
    if end_event_name: pretty_schedule[end_event_name] = pretty_schedule.pop('end-plan')

    return pretty_schedule

def schedule_plan(start_event, stn):
    """Computes a fixed schedule offline that satisfies all of the
    temporal constraints in the STN.
    
    Input: stn - a networkx DiGraph with simple temporal constraints
           start_event - a string, the event name in stn of the start event.
                         This start event should always be scheduled at t=0.
                         
    Output: A schedule, which should take the form of a dictionary. The dictionary should
            map event names in the stn to the time at which they are executed. For example,
            {'A': 0.0, ...}
    """
    
    g_distance = convert_stn_to_distance_graph(stn)
    g_disp = compute_apsp_dispatchable_form(g_distance)

    # Set an empty schedule so far
    schedule = {}
    # Initialize execution windows
    execution_windows = {}
    for e in g_disp.nodes():
        execution_windows[e] = ExecutionWindow(-np.inf, np.inf)
        
    # Schedule the first event
    schedule[start_event] = 0.0
    propagate_to_neighbors(start_event, 0.0, g_disp, execution_windows)
    
    for ei in (e for e in g_disp.nodes() if e != start_event):
        # Schedule ei
        # Choose any time in its execution window
        win = execution_windows[ei]
        alpha = np.random.random()
        ti = win.lb + alpha * (win.ub - win.lb)
        # Schedule ei at ti
        schedule[ei] = ti
        propagate_to_neighbors(ei, ti, g_disp, execution_windows)

    
    return schedule
