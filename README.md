# RMPL Tutorial 1

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mit-mers%2Fjupyter%2Frmpl-tutorial-1/master)

Introduction to RMPL tutorial including basic modeling in RMPL, TPN visualization, and temporal consistency checking.

You can run this tutorial directly from your browser using Binder by using the button above. 

**NOTE:** All of your work and files will be erased after you end the Binder session or close the tab. If you would like to save your work, be sure to download your edited files or run the notebook locally.

### To run this tutorial locally (on Mac or Linux):

Make sure you have Python 3.6 installed.

Install the required Python packages:

```
pip install jupyter && pip install requirements.txt
```

Get the RMPL compiler and install:
```
wget -q https://groups.csail.mit.edu/mers/code/rmpl/rmpl-v0.2.0-alpha.1-linux-amd64.tar.gz
tar -zxvf rmpl-v0.2.0-alpha.1-linux-amd64.tar.gz
mv rmpl-v0.2.0-alpha.1-linux-amd64/rmplc /usr/local/bin/
```

In the tutorial directory, start the jupyter notebook:
```
jupyter notebook
```
A notebook should open automatically in your browser but if not you can manually open the links generated from this command.

### To run tutorial locally using Docker container and `repo2docker`:

`repo2docker` is what Binder uses to launch docker containers in a browser.

[Follow the instructions for installing `repo2docker`](https://repo2docker.readthedocs.io/en/latest/install.html)

Then build and launch the docker container:

```
jupyter-repo2docker <path-to-tutorial-repository>
```