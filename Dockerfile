FROM python:3.6-slim

# install python requirements
RUN pip install --no-cache --upgrade pip && \
    pip install --no-cache notebook

COPY requirements.txt .
RUN pip install --no-cache -r requirements.txt

# get RMPL compiler
RUN apt-get update && \
    apt-get install -y --no-install-recommends wget && \
    wget -q https://groups.csail.mit.edu/mers/code/rmpl/rmpl-v0.2.0-alpha.1-linux-amd64.tar.gz && \
    tar -zxvf rmpl-v0.2.0-alpha.1-linux-amd64.tar.gz && \
    mv rmpl-v0.2.0-alpha.1-linux-amd64/rmplc /usr/local/bin/ && \
    rm -rf rmpl-v0.2.0-alpha.1-linux-amd64.tar.gz && \
    rm -rf rmpl-v0.2.0-alpha.1-linux-amd64

# create user with a home directory
ARG NB_USER=jovyan
ARG NB_UID=1000
ENV USER ${NB_USER}
ENV HOME /home/${NB_USER}

RUN adduser --disabled-password \
    --gecos "Default user" \
    --uid ${NB_UID} \
    ${NB_USER}

# Make sure the contents of our repo are in ${HOME}
COPY *.ipynb *.rmpl ${HOME}/
COPY src ${HOME}/src/
COPY img ${HOME}/img
COPY solutions ${HOME}/solutions
COPY docs ${HOME}/docs

# Make sure user can access files and change working directory to home folder
USER root
RUN chown -R ${NB_UID} ${HOME}
USER ${NB_USER}
WORKDIR ${HOME}